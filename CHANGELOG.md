# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).



## 2.0.0 - 2019-03-03

### Changed
- updated symfony components to 3.4 
- [BC BREAK] minimum required php version to 7.0.8

### Deprecated
- support for php versions smaller than 7.0.8

## 1.0.2 - 2019-03-03

### Added
- Changelog.md
- lint command


## 1.0.1 - 2019-03-01

### Changed
- moved build file to the project root dir to produce build artifacts without directories inside 
- [ci] changed box version to build with to 2.7.5 because box 3.4.0 produces a broken build
- [ci] enabled phar building which was not required by box 3.4.0


## 1.0.0 - 2019-02-25

### Added
- version output according git tag
- composer.yaml, box.yaml
- [ci] gitlab-ci


---





<!---
### Added
- 

### Changed
- 

### Deprecated
- 

### Removed
- 

### Fixed
- 
-->
