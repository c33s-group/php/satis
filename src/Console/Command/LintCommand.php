<?php

declare(strict_types=1);

/*
 * This file is part of composer/satis.
 *
 * (c) Composer <https://github.com/composer>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Composer\Satis\Console\Command;

use Composer\Command\BaseCommand;
use Composer\Config;
use Composer\Config\JsonConfigSource;
use Composer\Json\JsonFile;
use Composer\Json\JsonValidationException;
use Composer\Satis\Builder\ArchiveBuilder;
use Composer\Satis\Builder\PackagesBuilder;
use Composer\Satis\Builder\WebBuilder;
use Composer\Satis\Console\Application;
use Composer\Satis\Console\Command\Traits\JsonCheckTrait;
use Composer\Satis\PackageSelection\PackageSelection;
use Composer\Util\RemoteFilesystem;

use Seld\JsonLint\ParsingException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


/**
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
class LintCommand extends BaseCommand
{
    use JsonCheckTrait;
    protected function configure()
    {
        $this
            ->setName('lint')
            ->setDescription('Validates the satis.json file against its schema.')
            ->setDefinition([
                new InputArgument('file', InputArgument::OPTIONAL, 'Json file to use', './satis.json'),
            ])
            ->setHelp(<<<'EOT'


EOT
            );
    }

    /**
     * @param InputInterface  $input  The input instance
     * @param OutputInterface $output The output instance
     *
     * @throws JsonValidationException
     * @throws ParsingException
     * @throws \Exception
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $configFile = $input->getArgument('file');
        $io = new SymfonyStyle($input, $output);
        $file = new JsonFile($configFile);
        
        if (!$file->exists()) {
            $io->error('File not found: "' . $configFile . '"');

            return 1;
        }

        try {
            $this->check($configFile);
        } catch (JsonValidationException $e) {
            foreach ($e->getErrors() as $error) {
                $io->error(sprintf('%s', $error));
            }
            $io->error(sprintf('%s: %s', get_class($e), $e->getMessage()));
            return 1;
        } catch (ParsingException $e) {
            $io->error(sprintf('%s: %s', get_class($e), $e->getMessage()));
            return 1;
        } catch (\UnexpectedValueException $e) {
            $io->error(sprintf('%s: %s', get_class($e), $e->getMessage()));
            return 1;
        }
        $io->success('File valid');

        return 0;
    }
}
